/**
 * Represents a circle 
 * (defined by its center point and radius)
 */
public class Circle {
	private static final double DEFAULT_CENTER_X = 0;
	private static final double DEFAULT_CENTER_Y = 0;
	private static final double DEFAULT_RADIUS = 1;
	private static final double GREATEST_IMPOSSIBLE_RADIUS = 0;
	private static final double GREATEST_IMPOSSIBLE_FACTOR = 0;


	private double centerX;
	private double centerY;
	private double radius;
	
	public Circle() {
		centerX = DEFAULT_CENTER_X;
		centerY = DEFAULT_CENTER_Y;
		radius = DEFAULT_RADIUS;
	}
	/**
	 * 
	 * @param x Circle's center X coordinate
	 * @param y Circle's center Y coordinate
	 * @param r Circle's radius
	 */
	public Circle(double x, double y, double r) {
		centerX = x;
		centerY = y;
		if (r <= GREATEST_IMPOSSIBLE_RADIUS) {
			radius = DEFAULT_RADIUS;
		} else {
			radius = r;
		}
	}
	/**
	 * @return area of circle
	 */
	public double area() {
		return Math.PI * Math.pow(radius, 2);
	}
	/**
	 * 
	 * @return perimeter of circle
	 */
	public double perimeter() {
		return 2 * Math.PI * radius;
	}
	/**
	 * 
	 * @param factor to resize circle by
	 */
	public void resize(double factor) {
		if (factor > GREATEST_IMPOSSIBLE_FACTOR) {
			radius *= factor;
		}
	}
	/**
	 * 
	 * @param x coordinate to move circle to
	 * @param y coordinate to move circle to
	 */
	public void moveTo(double x, double y) {
		centerX = x;
		centerY = y;	
	}
	/**
	 * 
	 * @param x coordinate of point
	 * @param y coordinate of point
	 * @return true if point is in or on perimeter of circle 
	 */
	public boolean isIn(double x, double y) {
		return (centerX - x)*(centerX - x) + (centerY - y)*(centerY - y) <= Math.pow(radius, 2);
	}
	public double getCenterX() {
		return centerX;
	}
	public void setCenterX(double centerX) {
		this.centerX = centerX;
	}
	public double getCenterY() {
		return centerY;
	}
	public void setCenterY(double centerY) {
		this.centerY = centerY;
	}
	public double getRadius() {
		return radius;
	}
	public void setRadius(double radius) {
		if (radius > GREATEST_IMPOSSIBLE_RADIUS) {
			this.radius = radius;
		}
	}
	public String toString() {
		return "Circle at ("+centerX+", "+centerY+") radius "+radius;
	}
}
