public class Ex9 {
	/**
	 * checks if 'a' and 'b' are 'swapEqual'
	 * 'swapEqual' is defined as two arrays that have had 0 or more 'mirror-swaps'
	 * 'mirror-swap' is defined as a swap the cells in indexes 'i' with 'a.length-i-1'
	 *  
	 * @param a
	 * @param b
	 * @return true if 'a' and 'b' are 'swapEqual'
	 */
	public static boolean isSwapEqual(int[] a, int[] b) {
		if (a.length != b.length)
			return false;
		return isSwapEqual(a, b, 0);
	}
	private static boolean isSwapEqual(int[] a, int[] b, int i) {
		if (i > a.length / 2) {
			return true;
		}
		if ((a[i] == b[i] && a[a.length - i - 1] == b[a.length - i - 1]) ||
			(a[i] == b[a.length - i - 1] && a[a.length - i - 1] == b[i])) {
			return isSwapEqual(a, b, i + 1);
		}
		return false;
	}
	
	/**
	 * prints all binary numbers that can be represented by 'n' digits
	 * @param n
	 */
	public static void printAllBinary(int n) {
		printAllBinary(n, "");
	}
	private static void printAllBinary(int n, String s) {
		if (n == 0) {
			if (allOnes(s))
				System.out.print(s);
			else 
				System.out.print(s + ", ");
		}
		else {
			printAllBinary(n - 1, s + "0");
			printAllBinary(n - 1, s + "1");
		}	
	}

	/**
	 * Recursive helper function that returns true 
	 * when all characters of 's' string are '1'
	 * @param s
	 * @return
	 */
	private static boolean allOnes(String s) {
		return allOnes(s, 0);
	}
	private static boolean allOnes (String s, int curr) {
		if (s.length() == curr)
			return true;
		if (s.charAt(curr) == '1')
			return allOnes(s, curr + 1);
		return false;
	}
	/**
	 * Recursive function that prints all "sub-words" from 's' 
	 * "sub-words" are defined as sub-strings that the order of their letters is 
	 * the same as the order in 's'
	 * @param s
	 */
	public static void printSubs(String s) {
		printSubs(s.length(), s, "");
	}
	private static void printSubs(int sLength, String s, String total) {
		if (s.length() == 0) {
			if (total != "") {
				if (sLength == total.length()) 
					System.out.println("\"" + total + "\"");
				else
					System.out.print("\"" + total + "\", ");
			}
		}
		else {
			char c = s.charAt(s.length() - 1);
			printSubs(sLength, s.substring(0, s.length() - 1), total);
			printSubs(sLength, s.substring(0, s.length() - 1), c + total);
		}	
	}
}
