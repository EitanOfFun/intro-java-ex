public class Room {
	private static final String ROOM_TITLE = "Room type";
	private static final String AREA_TITLE = "Area";
	private static final double MIN_AREA = 0;
	
	private String type;
	private double area;
	
	public Room(String type, double area) {
		this.type = type;
		if (area < MIN_AREA)
			this.area = MIN_AREA;
		else {
			this.area = area;
		}
	}
	public Room(Room other) {
		this.type = other.type; 
		this.area = other.area;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public double getArea() {
		return area;
	}
	public void setArea(double area) {
		if (area < MIN_AREA)
			this.area = MIN_AREA;
		else {
			this.area = area;
		}
	}
	public String toString() {
		return ROOM_TITLE+": "+type+", "+AREA_TITLE+": "+area;
	}
}
