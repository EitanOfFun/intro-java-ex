public class Ex8 {	
	private static final char LOWERCASE_LOWER_BOUNDS = 'a';
	private static final char LOWERCASE_UPPER_BOUNDS = 'z';
	private static final char UPPERCASE_LOWER_BOUNDS = 'A';
	private static final char UPPERCASE_UPPER_BOUNDS = 'Z';
	private static final int LETTER_ASCII_DIFF = LOWERCASE_LOWER_BOUNDS - UPPERCASE_LOWER_BOUNDS;


	/**
	 *  
	 * @param num
	 * @return count of how many digits in num are even
	 */
	public static int howManyEven(int num) {
		if (num == 0) 
			return 0; 
		if ((num % 10) % 2 == 0)
			return 1 + howManyEven(num / 10);
		return howManyEven(num / 10);
	}
	
	/**
	 * returns length of longest ascending sequence in a
	 * @param a
	 * @return
	 */
	public int longestAscending(int[] a) {
		return 1 + longestAscending(a, 1, a[0]);
	}
	private int longestAscending(int[] a, int i, int currentMax) {
		if (a.length == i)
			return 0;
		if (a[i] > currentMax) 
			return 1 + longestAscending(a, i + 1, a[i]);
		return longestAscending(a, i + 1, currentMax);
	}
	/**
	 * Merge two sorted arrays into a new sorted array
	 * @param a
	 * @param b
	 * @return
	 */
	public int[] merge(int[] a, int[] b) {
		int[] merged = new int[a.length + b.length];
		return merge(a, b, 0, 0, merged);
	}
	private  int[] merge(int[] a, int[] b, int aI, int bI, int[] merged) {
		if (a.length == aI && b.length == bI) 
			return merged;
		if (a.length == aI) {
			merged[aI + bI] = b[bI];
			return merge(a, b, aI, bI + 1, merged);
		}
		if (b.length == bI) {
			merged[aI + bI] = a[aI];
			return merge(a, b, aI + 1, bI, merged);
		}
		if (a[aI] < b[bI]) {
			merged[aI + bI] = a[aI];
			return merge(a, b, aI + 1, bI, merged);
		}
		merged[aI + bI] = b[bI];
		return merge(a, b, aI, bI + 1, merged);		
	}
	
	/**
	 * compare equality of characters ignoring difference between upper and lower case
	 * @param s1
	 * @param s2
	 * @return
	 */
	public boolean equalIgnoreCase(String s1, String s2) {
		if (s1.length() != s2.length())
			return false;
		if (s1.length() == 0)
			return true;
		
		char c1 = s1.charAt(0);
		char c2 = s2.charAt(0);
		
		if (c1 == c2) 
			return equalIgnoreCase(s1.substring(1), s2.substring(1));
		
		if ((c1 >= UPPERCASE_LOWER_BOUNDS && c1 <= UPPERCASE_UPPER_BOUNDS)|| 
			(c1 >= LOWERCASE_LOWER_BOUNDS && c1 <= LOWERCASE_UPPER_BOUNDS) &&
			(c2 >= UPPERCASE_LOWER_BOUNDS && c2 <= UPPERCASE_UPPER_BOUNDS) ||
			(c2 >= LOWERCASE_LOWER_BOUNDS && c2 <= LOWERCASE_UPPER_BOUNDS)) {
			int diff = c1 - c2;
			if (diff == LETTER_ASCII_DIFF || diff == -LETTER_ASCII_DIFF)
				return equalIgnoreCase(s1.substring(1), s2.substring(1));
		}
		return false;
	}
}
