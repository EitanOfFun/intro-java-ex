/**
 * represents a linear equation in the form of
 * y = ax + b
 * 	
 */
public class Linear {
	private double a;
	private double b;
	
	public Linear(double a, double b) {
		this.a = a;
		this.b = b;
	}
	/**
	 * Copy constructor
	 * @param lin
	 */
	public Linear(Linear lin) {
		a = lin.getA();
		b = lin.getB();
	}
	public double getA() {
		return a;
	}
	public double getB() {
		return b;
	}
	/**
	 * assigns x value and returns solved equation for y
	 * @param x
	 * @return
	 */
	public double assign(double x) {
		return a * x + b;
	}
	/**
	 * assigns y value and returns solved equation for x
	 * @param 4 = ax + b
	 * @return
	 */
	public double solve(double y) {
		return (y - b) / a;
	}
	/**
	 * 
	 * @param other
	 * @return
	 */
	public double getIntersection(Linear other) {
		return new Linear(a - other.getA(), b - other.getB())
				.solve(0);
	}
	/**
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean isOnLine(double x, double y)  {
		return y == a*x + b;
	}
	/**
	 * 
	 * @param other line
	 * @return true if the two lines are parallel
	 */
	public boolean areParallel(Linear other) {
		return a == other.getA();
	}
	/**
	 * 
	 * @param x1 coordinate
	 * @param y1 coordinate
	 * @param x2 coordinate
	 * @param y2 coordinate
	 * @return linear equation from two points
	 */
	public Linear createLinear(double x1, double y1, double x2, double y2) {
		final double m = (y2 - y1) / (x2 - x1);
		final double b = -m*x1 + y1;
		return new Linear(m, b);
	}
	/**
	 * helper method
	 * @param a
	 * @return formats a for toString
	 */
	private static String formatA(double a) {
		if (a == 0) {
			return "";
		}
		if (a == 1) {
			return "x";
		}
		if (a == -1) {
			return "-x";
		}
		return a + "x";
	}
	/**
	 * helper method
	 * @param b
	 * @return formats b for toString
	 */
	private static String formatB(double a, double b) {
		if (a == 0) {
			return "" + b;
		}
		if (b == 0) {
			return "";
		}
		if (b < 0) {
			return " " + b;
		}
		return " + " + b;
	}
	/**
	 * string representation of equation
	 */
	public String toString() {
		if (a == 0 && b == 0) {
			return "y = 0";
		}
		return "y = " + formatA(a) + formatB(a, b);
	}
}
