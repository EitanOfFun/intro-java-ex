public class Apartment {
	private static final int LOWEST_FLOOR = 0;
	private static final int STARTING_NUM_OF_ROOMS = 0;
	
	private String family;
	private int flat;
	private Room firstRoom;
	private Room secondRoom;
	private Room thirdRoom;
	private int numOfRooms;
	
	public Apartment(String name, int flat) {
		this.family = name;
		if (flat < LOWEST_FLOOR) {
			flat = LOWEST_FLOOR;
		} else {
			this.flat = flat;
		}
		firstRoom = null;
		secondRoom = null;
		thirdRoom = null;
		numOfRooms = STARTING_NUM_OF_ROOMS;
	}
	/**
	 * 
	 * @param type of room to return
	 * @return room with type argument
	 */
	public Room getRoomByType(String type) {
		if (firstRoom != null && firstRoom.getType().equals(type)) {
			return new Room(firstRoom);
		} if (secondRoom != null && secondRoom.getType().equals(type)) {
			return new Room(secondRoom);
		} if (thirdRoom != null && thirdRoom.getType().equals(type)) {
			return new Room(thirdRoom);
		}
		return null;
	}
	public String getFamily() {
		return family;
	}
	public int getFlat() {
		return flat;
	}
	public int getNumOfRooms() {
		return numOfRooms;
	}
	/**
	 * if room is available sets available room to r
	 * if not, doesn't do anything
	 * (increases numOfRooms if set succeeded)
	 * @param r room to set
	 */
	public void setRoom(Room r) {
		if (firstRoom == null) {
			firstRoom = new Room(r);
			numOfRooms++;
		} else if (secondRoom == null) {
			secondRoom = new Room(r);
			numOfRooms++;
		} else if (thirdRoom == null) {
			thirdRoom = new Room(r);
			numOfRooms++;
		}
	}
	/** 
	 * helper method for getTotalArea function
	 * @param r
	 * @return
	 */
	private static double getRoomArea(Room r) {
		if (r == null)
			return 0;
		return r.getArea();
	}
	/**
	 * 
	 * @return total area of apartment
	 */
	public double getTotalArea() {
		return getRoomArea(firstRoom) + getRoomArea(secondRoom) + getRoomArea(thirdRoom);
	}
	/**
	 * String representation of apartment
	 */
	public String toString() {
		String s = family+"'s apartment, flat "+flat+" has "+numOfRooms+" rooms";
		if (firstRoom != null) {
			s += "\n" + firstRoom.toString();
		} 
		if (secondRoom != null) {
			s += "\n" + secondRoom.toString();
		} 
		if (thirdRoom != null) {
			s += "\n" + thirdRoom.toString();
		}
		return s;
	}
}
