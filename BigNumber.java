/**
 * This class represents an arbitrarily large number
 * It allows you to multiple two BigNumbers add two BigNumbers and to print a BigNumber
 * NOTE: This class can multipy and add numbers only limited by your RAM.
 * It can even multiply numbers with more digits that 2^63 (Long.MAX_VALUE)
 */
public class BigNumber {
    private final static int DEFAULT_VALUE = 0;
    private IntNode head;
    /**
     * String Constructor if 'num' is not a String of numbers or is empty or null then
     * It will construct the number 0
     * @param num
     */
    public BigNumber(String num) {
        if (num == null || num.length() == 0) {
            head = new IntNode(DEFAULT_VALUE, null);
            return;
        }
        else {
            if (isDigit(num.charAt(0)))
                head = new IntNode(Character.getNumericValue(num.charAt(0)), null);
        }
        for (int i = 1; i < num.length(); i++) {
            char c = num.charAt(i);
            if (isDigit(c))
                head = new IntNode(Character.getNumericValue(c), head);
            else {
                head = new IntNode(DEFAULT_VALUE, null);
                return;
            }
        }
    }
    /**
     * Copy Constructor creates a new BigNumber copying all IntNodes to avoid aliasing
     * @param b
     */
    public BigNumber(BigNumber b) {
        head = new IntNode(b.head.getValue(), null);
        IntNode pb = b.head.getNext();
        IntNode p = head;
        while (pb != null) {
            p.setNext(new IntNode(pb.getValue(), null));
            p = p.getNext();
            pb = pb.getNext();
        }
    }
    /**
     * Adds this BigNumber by the arg BigNumber 'other'
     * NOTE: Returns a NEW BigNumber and doesn't modify any provided BigNumber's
     * @param other
     * @return
     */
    public BigNumber add(BigNumber other) {
        BigNumber res = new BigNumber("0");

        int d1 = getSafeVal(head);
        int d2 = getSafeVal(other.head);

        int s = (d1 + d2) % 10;
        int cin = (d1 + d2) / 10;

        res.head = new IntNode(s,  null);

        IntNode pRes = res.head;
        IntNode p1 = head.getNext();
        IntNode p2 = other.head.getNext();

        for (; p1 != null || p2 != null; pRes = pRes.getNext()) {
            d1 = getSafeVal(p1);
            d2 = getSafeVal(p2);

            s = (d1 + d2 + cin) % 10;
            cin = (d1 + d2 + cin) / 10;

            pRes.setNext(new IntNode(s, null));

            if (p1 != null)
                p1 = p1.getNext();
            if (p2 != null)
                p2 = p2.getNext();
        }
        if (cin == 0)
            return res;

        pRes.setNext(new IntNode(cin, null));
        return res;
    }
    /**
     * Adds this BigNumber by the given long - 'n'
     * NOTE: Returns a NEW BigNumber and doesn't modify any provided BigNumber's
     * @param n
     * @return
     */
    public BigNumber add(long n) {
        return add(new BigNumber(Long.toString(n)));
    }
    /**
     * Returns a new BigNumber that is the current BigNumber + 1
     * @return
     */
    public BigNumber plusplus() {
        return add(1);
    }
    /**
     * Multiplies this BigNumber by the arg BigNumber 'other'
     * NOTE: Returns a NEW BigNumber and doesn't modify any provided BigNumber's
     * @param other
     * @return
     */
    public BigNumber multiply(BigNumber other) {
        BigNumber res = multDigit(other.head.getValue(), this);
        IntNode p = other.head.getNext();
        BigNumber zeros = new BigNumber("0");
        // O(n^2)
        for (IntNode lastZero = zeros.head; p != null; p = p.getNext(), lastZero = lastZero.getNext()) {
            BigNumber temp = multDigit(p.getValue(), this);
            lastZero.setNext(temp.head);
            res = res.add(zeros);
            lastZero.setNext(new IntNode(0, null));
        }
        reverse(res); //O(n)
        removeLeadingZeros(res); //O(n)
        reverse(res); //O(n)
        return res;
    }
    /**
     * Multiplies this BigNumber by the given long - 'n'
     * NOTE: Returns a NEW BigNumber and doesn't modify any provided BigNumber's
     * @param n
     * @return
     */
    public BigNumber mult(long n) {
        return multiply(new BigNumber(Long.toString(n)));
    }
    /**
     * return's string representation of number, add's commas every 3 digits
     * @return
     */
    public String toString() {
        String s = "";
        IntNode p = head;
        for (int i = 1; p != null; i++, p = p.getNext()) {
            if (i % 3 == 0 && p.getNext() != null)
                s = "," + p.getValue() + s;
            else
                s = p.getValue() + s;
        }
        return s;
    }
    /**
     * Helper function to multiple a single digit by a big number
     * represents one row in the list of rows to be added at the end
     * @param digit
     * @param b
     * @return a new BigNumber that is the multiplication of the digit by 'b'
     */
    private static BigNumber multDigit(int digit, BigNumber b) {
        BigNumber res = new BigNumber("");

        int s = digit * b.head.getValue() % 10;
        int cin = digit * b.head.getValue() / 10;

        res.head = new IntNode(s,  null);

        IntNode pb = b.head.getNext(), pRes = res.head;

        for (; pb != null; pRes = pRes.getNext(), pb = pb.getNext()) {
            s = (digit * pb.getValue() + cin) % 10;
            cin = (digit * pb.getValue() + cin) / 10;
            pRes.setNext(new IntNode(s, null));
        }
        if (cin != 0)
            pRes.setNext(new IntNode(cin, null));
        return res;
    }
    /**
     * Helper function to determine if a character is a digit (0 - 9)
     * @param c
     * @return
     */
    private static boolean isDigit(char c) {
        return c >= '0' && c <= '9';
    }
    /**
     * Helper function for 'add' method if 'n' is null returns 0, else returns value
     * @param n
     * @return
     */
    private static int getSafeVal(IntNode n) {
        if (n == null)
            return 0;
        return n.getValue();
    }
    /**
     * Reverses the number (taken from lesson's PowerPoint)
     * @param b
     */
    private static void reverse(BigNumber b) {
        IntNode rev, curr;
        rev = null;
        curr = b.head;
        while(curr != null) {
            b.head = b.head.getNext();
            curr.setNext(rev);
            rev = curr;
            curr = b.head;
        }
        b.head = rev;
    }
    /**
     * Helper function to remove leading zeros from number
     * e.g. : 123000 ==> 123
     * @param b
     */
    private static void removeLeadingZeros(BigNumber b) {
        IntNode p = b.head;
        while (p != null && p.getValue() == 0 && p.getNext() != null)
            p = p.getNext();
        b.head = p;
    }
}
