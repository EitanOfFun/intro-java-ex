import java.util.Arrays;

/**
 * This class represents an Array of integers, and provides different operations on it
 */
public class Array {
	private int[] numbers;
	/**
	 * Constructor, deep copies array to avoid aliasing)
	 * @param array
	 */
	public Array(int[] array) {
		this.numbers = copyArray(array, 0, array.length);
	}
	/**
	 * @param other
	 * @return true if other is a prefix to this array
	 */
	public boolean isPrefix(Array other) {
		if (other.numbers.length > numbers.length) {
			return false;
		}
		for (int i = 0; i < other.numbers.length ; i++) {
			if (numbers[i] != other.numbers[i]) {
				return false;
			}
		}
		return true;
	}
	/**
	 * @param other
	 * @return true if other is a suffix to this array
	 */
	public boolean isSuffix(Array other) {
		if (other.numbers.length > numbers.length) {
			return false;
		}
		for (int i = 0; i < other.numbers.length; i++) {
			if (numbers[numbers.length - 1 - i] != other.numbers[other.numbers.length - 1 - i]) {
				return false;
			}
		}
		return true;
	}
	/**
	 * 
	 * @param other
	 * @return longest common prefix between the two arrays
	 */
	public Array longestCommonPrefix(Array other) {
		for (int i = other.numbers.length; i >= 1; i--) {
			Array currentPrefix = copyArray(other, i);
			if (isPrefix(currentPrefix)) {
				return currentPrefix; 
			}
		}
		return new Array(new int[]{});
	}
	/**
	 * 
	 * @param other
	 * @return longest common suffix between the two arrays
	 */
	public Array longestCommonSuffix(Array other) {
		for (int i = 0; i < other.numbers.length; i++) {
			Array currentSuffix = copyArray(other, i, other.numbers.length);
			if (isSuffix(currentSuffix)) {
				return currentSuffix; 
			}
		}
		return new Array(new int[]{});
	}
	/**
	 * 
	 * @return length of the longest Prefix AND suffix
	 */
	public int longestPrefixAndSuffix() {
		for (int i = numbers.length - 1; i >= 0; i--) {
			Array currentArray = new Array(Arrays.copyOf(numbers, i));
			if (isPrefix(currentArray) && isSuffix(currentArray)) {
				return i;
			}
		}
		// should never reach here
		return 0;
	}
	/**
	 * adds the provided array to the end of the current array and returns result
	 * @param other array to join with current
	 * @return united Array
	 */
	public Array uniteWith(Array other) {
		return new Array(joinArrays(numbers, other.numbers));
	}
	/**
	 * Removes the (first instance) of the given array from current array
	 * MUTATES current array!
	 * 
	 * @param other
	 */
	public void removeSub(Array other) {
		if (other.numbers.length <= numbers.length) {
			for (int i = 0; i <= numbers.length - other.numbers.length; i++) {
				if (isIn(numbers, other.numbers, i)) {
					numbers  = removeSub(this, i, i + other.numbers.length).numbers;
					return;
				}
			}
		}
	}
	/**
	 * Helper method to join two Arrays and return result
	 * @param a
	 * @param b
	 * @return a new joined array from a and b
	 */
	public static Array joinArrays(Array a, Array b) {
		return new Array(joinArrays(a.numbers, b.numbers));
	}
	@Override
	public String toString() {
		String s = Arrays.toString(numbers);
		s = s.substring(1, s.length() - 1);
		return '{' + s + '}';
	}
	/**
	 * Helper method to check if 'b' IS IN 'a' starting from a certain index in 'a'
	 * @param a
	 * @param b
	 * @param startIndex
	 * @return true if ALL of 'b' IS IN 'a'
	 */
	public static boolean isIn(int[]a, int[]b, int startIndex) {
		if (b.length > a.length || a.length < startIndex + b.length) {
			return false;
		}
		for (int i = 0; i < b.length; i++) {
			if (a[startIndex] != b[i]) {
				return false;
			}
			startIndex++;
		}
		return true;
	}
	/**
	 * Helper method for removing a sub array starting from a certain index and ending at another
	 * (Assumes 'start' and 'end' are within range)
	 * @param original array to remove from
	 * @param start index to remove from
	 * @param end index to remove to
	 * @return new Array with sub removed
	 */
	private static Array removeSub(Array original, int start, int end) {
		Array startArray = copyArray(original, start);
		Array endArray = copyArray(original, end, original.numbers.length);
		return joinArrays(startArray, endArray);
	}
	
	/**
	 * Helper method to return a sub primitive array from a given index to another index 
	 * @param original
	 * @param from
	 * @param to
	 * @return new sub array 
	 */
	private static int[] copyArray(int[] original, int from, int to) {
		int newLength = to - from;
		final int[] newNumbers = new int[newLength];
		for (int i = from, j = 0; i < to; i++, j++) {
			newNumbers[j] = original[i];
		}
		return newNumbers;
	}
	/**
	 * Helper method to create a sub array from the START of the array to a given index
	 * @param original
	 * @param to
	 * @return
	 */
	private static Array copyArray(Array original, int to) {
		return new Array(copyArray(original.numbers, 0, to));
	}
	/**
	 * 
	 * @param original
	 * @param from
	 * @param to
	 * @return
	 */
	private static Array copyArray(Array original, int from, int to) {
		return new Array(copyArray(original.numbers, from, to));
	}
	/**
	 * Helper method for joining two primitive arrays
	 * @param a
	 * @param b
	 * @return a + b (concatenation)
	 */
	private static int[] joinArrays(int[] a, int[] b) {
		int[] c = new int[a.length + b.length];
		for (int i = 0; i < a.length; i++) {
			c[i] = a[i];
		}
		for (int i = 0; i < b.length; i++) {
			c[i + a.length] = b[i];
		}
		return c;
	}
}
