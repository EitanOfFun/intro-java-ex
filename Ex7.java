public class Ex7 {
	private static final int NOT_FOUND = -1;
	/**
	 * Question 1
	 * I search from the top right, if the character is less than current, 
	 * I remove an entire row, if the character is greater than current,
	 * I remove an entire column. It kind of looks like a diagonal search across.
	 * This means it is in O(n)
	 * @param a
	 * @param x
	 * @return
	 */
	public static boolean find(int[][] a, int x){
		int i = 0, j = a.length - 1;
		while (j >= 0 && i < a.length) {
			if (x == a[i][j])
				return true;
			if (x < a[i][j])
				j--;
			else 
				i++;
		}
		return false;
	}
	/**
	 * Question 2
	 * I check if the result is in the even indexes, if yes I return it, if not, 
	 * I check if the result is in the even indexes, if yes I return it, if not,
	 * I return NOT_FOUND
	 * I check the indexes using a modified binary search which means the algo is
	 * in O(2logn) which is O(logn)
	 * @param a
	 * @param x
	 * @return
	 */
	public static int find(int[] a, int x) {
		int resultEven = find(a, x, true);
		if (resultEven != NOT_FOUND)
			return resultEven;
		int resultOdd = find(a, x, false);
		if (resultOdd != NOT_FOUND)
			return resultOdd;
		return NOT_FOUND;
	}
	/**
	 * Helper method for Question 2
	 * Modified binary search. searches even or odd indexes depending on 'even' param
	 * O(logn)
	 * @param a
	 * @param x
	 * @param even whether we are searching the even or odd indexes
	 * @return
	 */
	private static int find(int[] a, int x, boolean even) {
		int top = roundTo(a.length - 2, even);
		int bottom = roundTo(0, even);
		int mid;
		while (top >= bottom) {
			mid = roundTo((top + bottom) / 2, even);
			if (even) {
				if(x > a[mid])
					bottom = roundTo(mid + 2, even);
				else if(x < a[mid])
					top = roundTo(mid - 2, even);
				else 
					return mid;
			}
			else {
				if(x > a[mid])
					top = roundTo(mid - 2, even);
				else if(x < a[mid])
					bottom = roundTo(mid + 2, even);
				else
					return mid;
			}
		}
		return NOT_FOUND;
			
	}
	/**
	 * Hepler method to round numbers to an even or odd number
	 * @param a original number
	 * @param even - true, or false for odd
	 * @return rounded number
	 */
	private static int roundTo(int a, boolean even) {
		if (a % 2 == 0 == even)
			return a;
		return a + 1;
	}
	/**
	 * Quesiton 3
	 * I count the number of times I reach a 'first' char,
	 * When I reach a 'last' char I add the amount of 'first' chars to my total
	 * I loop over the strings chars once. (twice really, but its a constant)
	 * O(n)
	 * @param s
	 * @param first
	 * @param last
	 * @return
	 */
	public static int countSub(String s, char first, char last){
		int firstCount = 0, count = 0;
		s = filter(s, first);
		
		for (int i = 0; i < s.length(); i++) {
			if(s.charAt(i) == first)
				firstCount++;
			if(s.charAt(i) == last)
				count += firstCount;
		}
		if (first == last)
			return count - firstCount;
		return count;
	}
	/**
	 * Helper method for Question 3
	 * Filters the string to start with a 'first' character
	 * O(n)
	 * @param s
	 * @param first
	 * @return
	 */
	private static String filter(String s, char first) {
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == first)
				return s.substring(i);
		}
		return "";
	}
}
