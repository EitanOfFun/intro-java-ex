import java.util.Scanner;
public class Numbers {

	private static final int ARMSTRONG_POWER = 3;
	private static final int MAX_VALID_DIGITS = 5;
	private static final int MIN_VALID_DIGITS = 3;

	public static void main(String[] args) {
		Scanner scan  = new Scanner(System.in);
		System.out.println("Please enter a number:");
		final int n = scan.nextInt();

		scan.close();	
		
		// if number is not valid
		// i.e. is not 3, 4, or 5 digits long
		if (n < Math.pow(10, MIN_VALID_DIGITS - 1) || n >= Math.pow(10, MAX_VALID_DIGITS - 1)) {
			System.out.println("The number " + n + " is invalid");
		} else {
			// The number is valid
			// Check if the number is 3 digits long
			if (n >= Math.pow(10, 2) && n < Math.pow(10, 3)) {
				// number is 3 digits long
				// split number into digits
				final int d1 = n % 10;
				final int d2 = n / 10 % 10;
				final int d3 = n / 100;
				
				// is number an Armstrong number?
				if (Math.pow(d1, ARMSTRONG_POWER) + Math.pow(d2, ARMSTRONG_POWER) + Math.pow(d3, ARMSTRONG_POWER) == n) {
					System.out.println("The number " + n + " is an Armstrong number.");
				} else {
					System.out.println("The number " + n + " is not an Armstrong number.");
				}
			} else {
				if (n >= Math.pow(10, 3) && n < Math.pow(10, 4)) {
					// number is 4 digits long
					final int d1 = n % 10;
					final int d2 = n / 10 % 10;
					final int d3 = n / 100 % 10;
					final int d4 = n / 1000;
					
					// is number palindrome?
					if (d1 == d4 && d2 == d3) {
						System.out.println("The number " + n + " is a palindrome.");
					} else {
						System.out.println("The number " + n + " is not a palindrome.");
					}
				} else {
					// number is 5 digits long
					final int d1 = n % 10;
					final int d2 = n / 10 % 10;
					final int d4 = n / 1000 % 10;
					final int d5 = n / 10000;
					
					// is number palindrome?
					if (d1 == d5 && d2 == d4) {
						System.out.println("The number " + n + " is a palindrome.");
					} else {
						System.out.println("The number " + n + " is not a palindrome.");
					}
				}
			}
		}
	}
}
