/**
 * 1. Special Mult
 * 2. Print with Comma
 * 
 * Comments for checker:
 * Shai allowed me to use recursive solutions instead of iterative ones.
 * Shai also allowed me to have a "stack overflow" at too high n's
 * 	Note: On my computer I get a stack overflow at n == 31 but it may be lower on yours
 * 		  I hope it is clear that my solution works in principle and in theory,
 * 		  and would work on a computer with more resources or in a language with 
 * 		  tail-call optimization.  
 */
public class Loops {
	private static final int TOP_BOUNDS_IMPOSSIBLE_SEPARATER_DISTANCE = 0;
	private static final int TOP_BOUNDS_IMPOSSIBLE_TO_PRINT_NUMBERS = 0;
	private static final int UNDEFINED_SMULT = 11;
	private static final int UNDEFINED_RETURN_VALUE = 0;
	private static final String SEPARATER = ",";
	
	/**
	 * Defined as the smallest number that has the following 3 properties:
	 * 1. is a multiple of n
	 * 2. ends in n
	 * 3. sumOfDigits(smult) == n
	 * 
	 * @param n
	 * @return smult
	 */
	public static int specialMult(int n) {
		if (n == UNDEFINED_SMULT) 
			return UNDEFINED_RETURN_VALUE;
		// start recursive function at n (the smallest possible smult)
		return specialMult(n, n);
	}
	/**
	 * recursive helper function
	 * if found will return num
	 * if not will recursively try the next multiple of n until found
	 * @param num possible smult value (must be multiple of n)
	 * @param n 
	 * @return smult
	 */
	private static int specialMult(int num, int n) {
		if (doesEndWith(num, n) && sumOfDigits(num) == n) 
			return num;
		return specialMult(num + n, n);
	}
	/**
	 * recursive helper function to check if a num ends in n
	 * checks if first digits are equal, if they are:
	 * continues to check next digits, if the aren't: returns false
	 * @param num 
	 * @param n
	 * @return true is it does false if it doesn't
	 */
	private static boolean doesEndWith(int num, int n) {
		if (n == 0)
			return true;
		return (num % 10 == n % 10 && doesEndWith(num / 10, n / 10));
	}
	/**
	 * recursive helper function to return the sum of digits of a given integer
	 * @param n
	 * @return sum of digits of n
	 */
	private static int sumOfDigits(int n) {
		if (n == 0)
			return 0;
		return (n % 10) + sumOfDigits(n / 10); 
	}
	/**
	 * recursive function to print all special mult's up to and including
	 * a given n. starting with 1
	 * @param n
	 */
	public static void printAllSpecialMult(int n) {
		printAllSpecialMult(n, 1);
	}
	/**
	 * recursive helper function to print all special mult's
	 * @param n print all up to and including
	 * @param i current special mult to print
	 */
	private static void printAllSpecialMult(int n, int i) {
		System.out.println(i + "\t" + specialMult(i));
		if (i != n)
			printAllSpecialMult(n, i + 1);
	}
	/**
	 * recursive function to print a number with a comma separating its digits
	 * @param n number to print
	 * @param d every number of digits to add a comma separator
	 */
	public static void printWithComma(int n, int d) {
		if (n <= TOP_BOUNDS_IMPOSSIBLE_TO_PRINT_NUMBERS || d <= TOP_BOUNDS_IMPOSSIBLE_SEPARATER_DISTANCE)
			System.out.println(n);
		else
			System.out.println(stringWithComma(n, d, 0));
	}
	/**
	 * recursive helper function that returns the string to print 
	 * @param n number left to delimit
	 * @param d every digits to insert a comma
	 * @param current digit number
	 * @return number with comma's separating it every d digits
	 */
	private static String stringWithComma(int n, int d, int current) {
		if (n == 0)
			return "";
		if (d == current)
			return stringWithComma(n / 10, d, 1) + n % 10 + SEPARATER;
		return stringWithComma(n / 10, d, current + 1) + n % 10;
	}
}
